; assembley to elf
; nasm -f elf64 ./helloworld.asm -o asmout.o -g
; ld ./asmoout.o -o asmout

section .data
section .text
global _start

_start:
jmp short label1

; calling
label:
pop rcx ; string from stack

; syscall "write"
xor rax, rax
mov al, 4
xor rbx, rbx
inc rbx
xor rdx, rdx
mov dl, 13
int 0x80

; syscall "exit"
xor rax, rax
inc rax
xor rbx, rbx
int 0x80

label1:
call label
db "Hello, world", 0x0a

; x32 on x64
; https://stackoverflow.com/questions/18429901/compiling-32-bit-assembly-on-64bit-system-ubuntu