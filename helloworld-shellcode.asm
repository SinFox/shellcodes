; assembly
; nasm ./helloworld-shellcode.asm -o ./sc

; disassembly
; ndisasm -b64 ./sc

BITS 64 ; 32

jmp short label1

; calling
label:
pop rcx ; string from stack

; syscall "write"
xor rax, rax
mov al, 4
xor rbx, rbx
inc rbx
xor rdx, rdx
mov dl, 13
int 0x80

; syscall "exit"
xor rax, rax
inc rax
xor rbx, rbx
int 0x80

label1:
call label
db "Hello, world", 0x0a
