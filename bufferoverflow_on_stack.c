// build 
// gcc -g -fno-stack-protector ./bufferoverflow_on_stack.c -o debug

#include <stdio.h>
#include <string.h>

void vuln(char *buf)
{
    char a[60];
    printf("%016x\n", &a);
    printf("%016x\n", buf);
    strcpy(a, buf);
}

int main(int argc, char *argv[])
{
    if (argc > 1)
        vuln(argv[1]);
    
    if (argc == 1)
    {
        printf("no argument\n");
    }
    
    return 0;
}
